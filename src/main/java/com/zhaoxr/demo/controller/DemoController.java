package com.zhaoxr.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @description:
 * @author:zhaojq
 * @date: 2019/10/23 16:08
 */
@RestController
public class DemoController {
    @GetMapping("/test")
    public Map<String, String> getDemo(){
        Map<String, String> returnMap = new HashMap<>();
        returnMap.put("demo","demo test");
        return returnMap;
    }
}
