FROM java:latest
VOLUME /tmp
COPY  target/demo-0.0.1-SNAPSHOT.jar app.jar
ENV PORT 9999
EXPOSE $PORT
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-Dserver.port=${PORT}","-jar","/app.jar"]

#FROM java:latest
#ADD target/demo-0.0.1-SNAPSHOT.jar /demo-0.0.1-SNAPSHOT.jar
#EXPOSE 9999
#ENTRYPOINT ["java", "-jar", "/demo-0.0.1-SNAPSHOT.jar"]
